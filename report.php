<?php
/**
 * Created by PhpStorm.
 * User: Aakash
 * Date: 04-10-2017
 * Time: 12:20
 */
session_start();
include "connection.php";
require 'FlashMessages.php';

$msg_pi = new \Plasticbrain\FlashMessages\FlashMessages();

if (isset($_GET['team_id_ajax'])) {
    $query = "select school_name,id from school_m where status=1 AND team_id =" . $_GET['team_id_ajax'] . " order by school_name";
    $result = mysqli_query($connect, $query);
    $school = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {

            $school[$row['school_name']] = $row['id'];
        }
    }
    echo json_encode($school);
    exit;
}
if (isset($_SESSION['user']) && $_SESSION['user'] == 'admin') {
    $query = "select team_name,id from team_m where status=1 order by team_name";
    $result = mysqli_query($connect, $query);
    $i = 1;
    $team = array();
    if ($result->num_rows > 0) {
        while ($row = mysqli_fetch_array($result)) {
            $team[$row['id']] = $row['team_name'];
        }
    }
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <link href="./bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="./css/mystyle.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
        <style>

        </style>
    </head>
    <body>
    <?php include "header.php" ?>

    <div class="container-fluid h-100">
        <div class="row h-100">
            <?php include "left_menu.php"; ?>

            <div class="col pt-2 col-md-10">
                <h2 class="form-signin-heading">PI Report</h2>
                <form class="form-signin  mt-10" method="post">
                    <div class="col-md-12 mt-5">

                        <div class="form-group">
                            <select class="form-control  float-left col-md-3" name="team_id" required id="team_id"
                                    value="<?php echo !empty($_GET['team_id']) ? $_GET['team_id'] : '' ?>">
                                <option value="">Team Name</option>
                                <?php foreach ($team as $key => $t) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $t; ?></option>
                                <?php } ?>

                            </select>

                            <select class="form-control  float-left col-md-3  ml-5" id="schools" name="school_id"
                                     value="<?php echo !empty($_GET['school_id']) ? $_GET['school_id'] : '' ?>">
                                <option value="">School Name</option>
                            </select>
                            <input type="submit" class="form-control btn btn-info col-md-2 ml-3" value="Search">

                        </div>
                    </div>

                </form>
                <?php

              //  $query_string='';
                if (!empty($_POST['team_id']) && !empty($_POST['school_id'])) {
                    $school_id = $_POST['school_id'];
                    $team_id = $_POST['team_id'];
                   // $query_string='?team_id='.$team_id;
                    $school_query = "SELECT pi_duration,invoice.igst,invoice.sgst,invoice.cgst,invoice.total,school_name,team_name,invoice.id as invoice_id,invoice.pi_no as pi_no,invoice.school_id as school_id FROM `school_m`  left join team_m on team_m.id=school_m.team_id right join invoice on invoice.school_id= school_m.id WHERE school_m.status=1 AND school_id=".$school_id." AND team_id=".$team_id;
                } elseif(!empty($_POST['team_id'])) {
                    $team_id = $_POST['team_id'];
                  //  $query_string='?team_id='.$team_id;
                    $school_query = "SELECT pi_duration,invoice.igst,invoice.sgst,invoice.cgst,invoice.total,school_name,team_name,invoice.id as invoice_id,invoice.pi_no as pi_no,invoice.school_id as school_id FROM `school_m`  left join team_m on team_m.id=school_m.team_id right join invoice on invoice.school_id= school_m.id WHERE school_m.status=1 AND team_id=".$team_id;
                }else {
                    $school_query = "SELECT pi_duration,invoice.igst,invoice.sgst,invoice.cgst,invoice.total,school_name,team_name,invoice.id as invoice_id,invoice.pi_no as pi_no,invoice.school_id as school_id FROM `school_m`  left join team_m on team_m.id=school_m.team_id right join invoice on invoice.school_id= school_m.id WHERE school_m.status=1";
                }
                //echo $school_query;die;
                $school_result = mysqli_query($connect, $school_query);
                $i = 1;
                $report=array();
                if ($school_result->num_rows > 0) {
                    $query = "SELECT school_code,pi_id,pi_no,team_name,school_name,amount_received,pi_duration,SUM(amount_received) as paid_total,SUM(bounce_penalty) as bounce_penalty,SUM(discount) as discount,igst,sgst,cgst,total FROM `payment` left join invoice on payment.pi_id=invoice.id left join school_m on school_m.id=invoice.school_id left join team_m on team_m.id=school_m.team_id  WHERE payment.status =1  group by pi_id";
                    $result = mysqli_query($connect, $query);
                    $pi_ids=array();
                    /*$pi_query = "SELECT id,grand_total from invoice where status=1;";
                    $pi_result = mysqli_query($connect, $pi_result);
                    while ($pi_row = mysqli_fetch_array($result)) {
                        $report[$pi_row['id']]=$pi_row;
                        $pi_ids[]=$pi_row['id'];
                    }*/
                   /* while ($row = mysqli_fetch_array($result)) {
                                $report[$row['school_code']][]=$row;
                      }*/
                     /*while ($row = mysqli_fetch_array($result)) {
                                 $report[$row['school_code']][]=$row;
                       }*/
                     while ($row = mysqli_fetch_array($result)) {
                                $pi_ids[]=$row['pi_id'];
                               $report[$row['pi_id']]=$row;
                       }
                }
                ?>
                <?php if ($msg_pi->hasMessages($msg_pi::SUCCESS)) { ?>
                    <div class="alert-success"><?php echo $msg_pi->display(); ?></div>
                <?php } else { ?>
                    <div class="alert-warning">
                        <?php echo $msg_pi->display(); ?>
                    </div>

                    <?php

                } ?>
<style>
  table,thead,tbody{
      border: 1px solid gray;
      font-size: 12px;
  }
    th,td,tr{
        border: 1px solid gray;
        font-size: 12px;

    }
</style>
                <?php $monthly_paid=[]; ?>
                <div class="col-md-12">
                    <table class="tbl">
                        <thead>
                        <tr>
                            <th>Team </th>
                            <th>School </th>
                            <th>Apr</th>
                            <th>May</th>
                            <th>Jun</th>
                            <th>July</th>
                            <th>Aug</th>
                            <th>Sep</th>
                            <th>Oct</th>
                            <th>Nov</th>
                            <th>Dec</th>
                            <th>Jan</th>
                            <th>Feb</th>
                            <th>Mar</th>
                            <th>Total Paid</th>
                            <th>Total Unpaid</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                      //  $report=array();
                        if ($school_result->num_rows > 0) {
                            $school_data=array();
                            $invoices=array();
                            while($school_row = mysqli_fetch_array($school_result)){
                                $school_data[$school_row['school_id']][]=$school_row;
                                $invoices[]=$school_row['invoice_id'];
                            }
                        /* echo "<pre>";
                           // print_r($pi_ids);

                            print_r($report);
                            print_r($school_data);
                            die;*/
                            $months=['Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb','Mar'];
                           $total_school_paid=0;
                           $total_school_unpaid=0;
                           $total_school_discount=0;
                           $total_school_bounce=0;
                            foreach ($school_data as $school_pis){
                                echo "<tr>";
                                echo "<td>" . $school_pis[0]['team_name'] . "</td>";
                                echo "<td>" . $school_pis[0]['school_name'] . "</td>";
                                $month=0;
                                $flag=0;
                                $total_paid=0;
                                $total_unpaid=0;
                                $total_discount=0;
                                $total_bounce=0;
/*echo "<pre>";
                                print_r($report);die;*/
                                for ($month=0;$month<12;$month++){
                                    $flag=0;
                                    echo "<td>";
                                    foreach ($school_pis as $key=>$school) {
                                        if (in_array($school['invoice_id'], $pi_ids)) {
                                            if (strtolower($report[$school['invoice_id']]['pi_duration']) == strtolower($months[$month])) {
                                               // $pay_total = number_format($report[$school['invoice_id']]['igst'] + $report[$school['invoice_id']]['sgst'] + $report[$school['invoice_id']]['cgst'] + $report[$school['invoice_id']]['total'],2);
                                                $pay_total = round($school['igst'] + $school['sgst'] + $school['cgst'] + $school['total']);
                                                $paid_total = round($report[$school['invoice_id']]['paid_total']);
                                                $discount = round($report[$school['invoice_id']]['discount']);

                                               // $bounce_panelty = round($report[$school['invoice_id']]['bounce_penalty']);

                                                $query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id IN (select id from invoice where pi_duration='" . Date("M", strtotime($report[$school['invoice_id']]['pi_duration'] . " last month")) . "' AND school_id IN (select id from school_m where school_code=  '" . $report[$school['invoice_id']]['school_code'] . "'))";
                                                $result_amount_pan = mysqli_query($connect, $query_amt_pan);

                                                $bounce_penalty = 0;
                                                if ($result_amount_pan->num_rows > 0) {
                                                    while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                                                        $bounce_penalty += $panelty_amt['bounce_penalty'];
                                                    }
                                                }

                                                $unpaid=$pay_total-$paid_total;
                                                $total_paid+=$paid_total;
                                                $total_unpaid+=$unpaid;
                                                $total_discount+=$discount;
                                                $total_bounce+=$bounce_penalty;
                                                echo "<span style=''> Bill:" . $pay_total . "</span><br>
                                                 <span style=''> Paid:" .$paid_total . "</span><br>
                                                 <span style=''> UnPaid:" .$unpaid . "</span><br>
                                                 <span style=''> Discount:" .$discount . "</span><br>
                                                 <span style=''> Bounce Pan:" .$bounce_penalty . "</span><br>
                                                 <span style=''> Month:" .$school['pi_duration'] . "</span><br>";
                                                $flag=1;
                                             //   unset($school_pis[$key]);
                                                $monthly_paid[$months[$month]]['paid'][]=$paid_total;
                                                $monthly_paid[$months[$month]]['unpaid'][]=$unpaid;
                                                $monthly_paid[$months[$month]]['discount'][]=$discount;
                                                $monthly_paid[$months[$month]]['bounce'][]=$bounce_penalty;
                                               // print_r($monthly_paid);
                                            }
                                        }
                                        elseif(strtolower($school['pi_duration']) == strtolower($months[$month])){
                                            $pay_total = round($school['igst'] + $school['sgst'] + $school['cgst'] + $school['total']);
                                            $paid_total=0.0;
                                            $discount = 0.0;
                                            $bounce_panelty = 0.0;

                                            $query_amt_pan = "select bounce_penalty from payment where status=1  AND pi_id IN (select id from invoice where pi_duration='" . Date("M", strtotime($school['pi_duration'] . " last month")) . "' AND school_id =  '" . $school['school_id'] . "')";
                                            $result_amount_pan = mysqli_query($connect, $query_amt_pan);

                                            $bounce_penalty = 0;
                                            if ($result_amount_pan->num_rows > 0) {
                                                while ($panelty_amt = mysqli_fetch_array($result_amount_pan)) {
                                                    $bounce_penalty += $panelty_amt['bounce_penalty'];
                                                }
                                            }

                                            $unpaid=$pay_total-$paid_total;
                                            $total_paid+=$paid_total;
                                            $total_unpaid+=$unpaid;
                                            $total_discount+=$discount;
                                            $total_bounce+=$bounce_penalty;
                                            echo "<span style=''> Bill:" . $pay_total . "</span><br>
                                                 <span style=''> Paid:" .$paid_total . "</span><br>
                                                 <span style=''> UnPaid:" .$unpaid . "</span><br>
                                                 <span style=''> Discount:" .$discount . "</span><br>
                                                 <span style=''> Bounce Pan:" .$bounce_penalty . "</span><br>
                                                 <span style=''> Month:" .$school['pi_duration'] . "</span><br>
                                                  ";
                                            $flag=1;
                                          //unset($school_pis[$key]);
                                            $monthly_paid[$months[$month]]['paid'][]=$paid_total;
                                            $monthly_paid[$months[$month]]['unpaid'][]=$unpaid;
                                            $monthly_paid[$months[$month]]['discount'][]=$discount;
                                            $monthly_paid[$months[$month]]['bounce'][]=$bounce_penalty;

                                        }elseif($flag==0){
                                         //   echo "<span style='font-size: 12px;font-weight: bold'> NA</span><br>";
                                            $flag=1;
                                        }


                                    }

                                    echo "</td>";

                                    /*if($flag==0)
                                        echo "</td>";
                                    else if($flag==1)
                                        echo "<span style='font-size: 12px;'>NA".$flag."</span></td>";*/

                                }
                                echo "<td>" . $total_paid . "</td>";
                                echo "<td>" . $total_unpaid . "</td>";
                                echo "</tr>";
                                $total_school_paid +=$total_paid;
                                $total_school_unpaid +=$total_unpaid;
                                $total_school_discount+=$total_discount;
                                $total_school_bounce+=$total_bounce;
                            }
                            echo "<tr>";
                            echo "<td >Total Paid</td>";
                            echo "<td></td>";
                            for($month=0;$month<12;$month++){
                                if(!empty($monthly_paid[$months[$month]])) {
                                    echo "<td><span> Paid:" . array_sum($monthly_paid[$months[$month]]['paid']) . "</span><br>
                                    <span> UnPaid:" . array_sum($monthly_paid[$months[$month]]['unpaid']) . "</span>
                                    <span> Discount:" . array_sum($monthly_paid[$months[$month]]['discount']) . "</span>
                                    <span> Bounce:" . array_sum($monthly_paid[$months[$month]]['bounce']) . "</span>
                                    ";
                                }else{
                                    echo "<td></td>";
                                }
                            }
                            echo "<td ><span> Paid:" . $total_school_paid . "</span><br>
                            <span> Discount:" . $total_school_discount . "</span>
                            <span> Bounce:" . $total_school_bounce . "</span> 
                            </td>";
                            echo "<td></td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<td>Total UnPaid</td>";
                            echo "<td></td>";
                            for($month=0;$month<12;$month++){
                                if(!empty($monthly_paid[$months[$month]])) {
                                    echo "<td> <span>" . array_sum($monthly_paid[$months[$month]]['unpaid']) . "</span>";
                                }else{
                                    echo "<td></td>";
                                }
                            }
                            echo "<td></td>";
                            echo "<td >".$total_school_unpaid."</td>";
                            echo "</tr>";

                            ?>

                            <?php

                        }
                       /* echo "<pre>";*/
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
        <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function(){
                $('.tbl').DataTable({
                    "paging": false
                });
            });
            $(document).ready(function () {
                $('#team_id').change(function () {
                    team_id = $("#team_id :selected").val();
                    $.ajax({
                        /* THEN THE AJAX CALL */
                        type: "GET", /* TYPE OF METHOD TO USE TO PASS THE DATA */
                        url: "pigenerate.php", /* PAGE WHERE WE WILL PASS THE DATA */
                        data: {team_id_ajax: team_id}, /* THE DATA WE WILL BE PASSING */
                        success: function (result) { /* GET THE TO BE RETURNED DATA */
                            result = JSON.parse(result);
                            var $select = $('#schools');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>School Name</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + value + '>' + key + '</option>');
                            });
                            /* THE RETURNED DATA WILL BE SHOWN IN THIS DIV */
                        }
                    });
                });

                /*$('#team_id').change(function () {
                    team_id = $("#team_id :selected").val();
                    $.ajax({
                        /!* THEN THE AJAX CALL *!/
                        type: "GET", /!* TYPE OF METHOD TO USE TO PASS THE DATA *!/
                        url: "pigenerate.php", /!* PAGE WHERE WE WILL PASS THE DATA *!/
                        data: {team_id_ajax: team_id}, /!* THE DATA WE WILL BE PASSING *!/
                        success: function (result) { /!* GET THE TO BE RETURNED DATA *!/
                            result = JSON.parse(result);
                            var $select = $('#schools');
                            $select.find('option').remove();
                            $select.append('<option value=' + "" + '>School Name</option>');
                            $.each(result, function (key, value) {
                                $select.append('<option value=' + key + '>' + value + '</option>');
                            });
                            /!* THE RETURNED DATA WILL BE SHOWN IN THIS DIV *!/
                        }
                    });
                });*/
            });
        </script>
    </div>
    <?php include "footer.php" ?>

    </body>
    </html>

    <?php
   /* if($query_string!=''){
        header("Location: report.php".$query_string);
        exit();
    }*/
} else {
    header("Location: index.php"); /* Redirect browser */
    exit();
}